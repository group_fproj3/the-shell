#include <ctype.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>



#ifndef __APASH_H__
#define __APASH_H__

int is_args;
char c;					//just so flush buffer code works.

/* inputbuffer and currentbuffer will hold the user's current commands, path has the current dir and is updated everytime
userinput loop re-iterates, prompt holds current prompt also updated, hostname also updated.  */
char inputbuffer[PATH_MAX], currentbuffer[PATH_MAX], currentdir[PATH_MAX], prompt[PATH_MAX], hostname[1024];

/* array of char* for holding args.*/
char inputargs[512][PATH_MAX];

//FUNCTIONS

int getInput();
void getPrompt();
void flushInput();
void findArguments();
void nullPointer(char *d);
void nullTerminate(char *s);
void printPipeArray();
void redirectionCheck();
int interpreter();
int buildArgArray();
int inputCheck(char * line);
int getPipeArray();
void freeVariables();
void getSpaces();
int changePath();
int deletePath();
int inputCheck();
void printArguments();
void printArgsArray();
void printPathVar();
void inputError1();
void inputError2();
void freeInputArgs();
int getWasFound();
int forFloppy();
void builtIn();
void thisISATest();
int forSystem();
#endif
