#include <ctype.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <floppy.h>
#include <apash.h>



/* Variables */
char name[9];
char ext[4];






char * formatName(char * in) {

	int i = 0,j = 0;
	int namecount = 0, extcount = 0;
	int k = 0;
	
	while (in[i] != '.' && i < 8) {
		name[i] = in[i];
//		printf("in[i]=%c\n",in[i]);
//		printf("i=%d\n",i);
		i++;
		
	}
	
	namecount = i;
	
	while (namecount < 8) {
		name[namecount] = ' ';
		namecount++;
	}
//	printf("namecount:%d\n", namecount);
	name[8] = '\0';
	
	
	
	i++; // skip '.'
	
	j = i + 3;
	while (in[i] != '\0' && i < j) {
		ext[k] = in[i];
		i++;
		k++;
	}
	
	extcount = k;
	
	while (extcount < 4) {
		ext[extcount] = ' ';
		extcount++;
	}
	
	ext[4] = '\0';
	
	
//	printf("name:%s\n", name);
	
	
	
//	printf("ext:%s\n", ext);
	
//	printf("extcount:%d\n", extcount);
//	
	strcat(name, ext);
//	printf("%s\n", name);
	
	return name;
}

/* mounts floppy image automatically as per Assignment 5 requirements */
void autoMount(){

	fd = open("imagefile.img", O_RDONLY,0);
	lseek(fd, 0,SEEK_SET);
	read(fd, currentBuffer,512);

	initializeFloppy();

}


/* mounts (opens) floppy image. */
/*void fmount(char * file) {
	if (!setfloppy){
	fd = open(file, O_RDONLY,0);
		lseek(fd, 0,SEEK_SET);
		read(fd, currentBuffer,512);
	}
	initializeFloppy();	
	setfloppy =1;
	f = file;
	printf("%s mounted.\n", (file));
}*/

/*closes filestream, clears set floppy status, erases floppyname */
/*void fumount() {
	close(floppyfilename);
	setfloppy =0;
	printf("%s unmounted.\n", floppyfilename);
	floppyfilename[0] = '\0';
}*/

/* will print floppy info via relevant STRUCT values */
void structure(){
	printf("\t\tnumber of FATs:   \t\t  %d\n", floppyA.numberoffats);
	printf("\t\tnumber of sectors used by FAT:    %d\n",floppyA.sectperfat );
	printf("\t\tnumber of sectors per cluster:    %d\n", floppyA.sectorpercluster);
	printf("\t\tnumber of ROOT entries:         %d\n",floppyA.maximumroot);
	printf("\t\tnumber of bytes per sector:     %d\n",SECTOR_SIZE);
	printf("\t\t---Sector #---     ---Sector Types---\n");
	printf("\t\t   0\t\t\t BOOT\n");
	printf("\t\t %d -- %d\t\t\t FAT1\n", floppyA.sectorpercluster, floppyA.sectperfat);
	printf("\t\t%d -- %d\t\t FAT2\n", floppyA.sectperfat+1, floppyA.sectperfat*2);
	printf("\t\t%d -- %d\t\t ROOT DIRECTORY\n", (floppyA.sectperfat*2)+1, (floppyA.sectperfat*2)+14);
}

 /* gets all the relevant floppy info from the boot sector of the volume. */
void initializeFloppy() {
	floppyA.numberoffats = currentBuffer[16];
	floppyA.sectperfat = currentBuffer[22];
	floppyA.sectorpercluster = currentBuffer[13];
	floppyA.bytespsector = currentBuffer[11];
	floppyA.maximumroot = currentBuffer[17];
	floppyA.totalnumofsectors = currentBuffer[19];
	floppyA.sectpertrack = currentBuffer[24];
	floppyA.resersector = currentBuffer[14];
	strncpy(floppyA.label, currentBuffer+43, 11); // gets volume label for floppyA.label (use sscanf or?)
	/* get size of sectors */
	SECTOR_SIZE = (currentBuffer[12]*256);

}

void help() {
	//printf("fmount [filename] - mount a local floppy disk\n");
	//printf("fumount - unmount the mounted floppy disk\n");
	printf("structure - list the structure of the floppy disk\n");
	printf("traverse - list the content in the root directory\n");
	printf("traverse -l - list the content in the root directory with additional details\n");
	printf("showfat - shows the first 256 entries in the FAT table\n");
	printf("showsector [number] - show the contents of given sector in the form of a hex dump\n");
	printf("showfile [filename] - show the contents of given file in the form of a hex dump\n");
	printf("quit - exit the shell\n");

}

char * getArguments(char * args) {
	char * arg2 = malloc(sizeof(*args));
	int len, m;
	int o = 1;
	len = (unsigned int)strlen(args);
	
	for (m = 0; m < len; m++){
		if(isspace(args[m])) {
			strncpy(arg2, (args+o), (len - m));	  // read (stringlength - current offset)
			return arg2;		
		} else {  o++; }	
			
	}
	return arg2;
}

char int2upperhex(int input) {
	char result;
	result = input / 16;
	if (result < 10)
		return result + 48;
	else if (result < 16)
		return result + 55;
}

char int2lowerhex(int input) {
	char result;
	result = input % 16;
	if (result < 10)
		return result + 48;
	else if (result < 16)
		return result + 55;
}

char u2l(char input) {
	if ((input >= 65) && (input <= 90))
		return input + 32;
	return input;
}

void showsector(int sectornumber) {
	unsigned char buf[1000];
	lseek(fd, sectornumber * 512, SEEK_SET);
	read(fd, buf, 512);

	printf("\n      0   1   2   3   4   5   6   7   8   9   A   B   C   D   E   F\n");

	int i;
	int counter = 0;
	int horizcounter = 16;
	char upper, lower, counterupperhex, counterlowerhex;

	for (i = 0; i < 512; i++) {
		if (horizcounter == 16) {
			horizcounter = 0;
			printf("\n");
			counterupperhex = int2upperhex(counter);
			counterlowerhex = int2lowerhex(counter);
			if (counterupperhex == '0')
				if (counterlowerhex == '0')
					printf("  0  ");
				else printf(" %c0  ", counterlowerhex);
			else printf("%c%c0  ", counterupperhex, counterlowerhex); 
			counter++;
		}
   
	upper = int2upperhex(buf[i]);
	lower = int2lowerhex(buf[i]);
	if (upper == '0')
		printf(" %c  ", lower);
	else printf("%c%c  ", upper, lower);
	horizcounter++;
   
	}
	printf("\n\n");
}

void showfat() {

	unsigned char buf[1000];

	lseek(fd, 10 * 512, SEEK_SET);

	read(fd, buf, SECTOR_SIZE);



	printf("\n          0    1    2    3    4    5    6    7    8    9    a    b    c    d    e    f");



	int i;

	int counter = 0;

	int horizcounter = 16;

	char upper, lower, counterupperhex, counterlowerhex;

	char one, two, three, four, five, six;

	char eight;

	for (i = 0; i < 384; i++) { 



		if (horizcounter == 16) {

			horizcounter = 0;

			//printf("");

			counterupperhex = int2upperhex(counter);

			counterlowerhex = int2lowerhex(counter);

			if (counterlowerhex == '0')

				printf("\n 0    ");

			else printf("\n%c0    ", counterlowerhex);

			

			if (counterlowerhex == '0') {

				printf("          ");

				horizcounter = horizcounter + 2;

			}

			counter++;

		}

   

		one = u2l(int2upperhex(buf[i]));

		two = u2l(int2lowerhex(buf[i]));

		three = u2l(int2upperhex(buf[i+1]));

		four = u2l(int2lowerhex(buf[i+1]));

		five = u2l(int2upperhex(buf[i+2]));

		six = u2l(int2lowerhex(buf[i+2]));

		//display 'FREE' if 000, remove hanging 0's

		if (i > 2) {

			if (four == '0')

				if (one == '0')

					if (two == '0')

						printf(" FREE");

					else printf("    %c", two);

				else printf("   %c%c", one, two);

			else printf("  %c%c%c", four, one, two);

			if (five == '0')

				if (six == '0')

					if (three == '0')

						printf(" FREE");

					else printf("    %c", three);

				else printf("   %c%c", six, three);

			else printf("  %c%c%c", five, six, three);

		horizcounter = horizcounter + 2;

		i = i + 2;

		}

   

	}

	printf("\n\n");

}


int string2Int(char * str) {

	int i, result = 0;

	int len = strlen(str);



	for(i=0; i<len; i++){



		result = result * 10 + ( str[i] - '0' );

	}



	return result;

}



void traverse(int sector) {

	int byteread = sector * SECTOR_SIZE;

	if (sector == 19)

		byteread += 32;

	char buf[1000000];

	lseek(fd, byteread, SEEK_SET);

	read(fd, buf, 14*SECTOR_SIZE);

	int i,j = 0,count = 0;

	char upper, lower;

	int subdir;

	int dotflag = 0;

	//printf("\n");

	for (i = 0; i < 14*SECTOR_SIZE; i++) {	

		if (buf[i] == 0)

			break;

		if (count == 0)

			printf("/");			

		if (buf[i] != 32) {

			printf("%c",buf[i]);

			if (buf[i] == 46) {

				if (buf[i+1] == 46) {

					printf("%c",buf[i+1]);

				}

				dotflag = 1;

				

				

			}

		}

		if (count == 7) {

			if ((buf[i+4] >> 4) % 2 == 1) {

				printf("\t\t\t\t\t<DIR>");

										

					printf("\n----SUBDIRECTORIES HERE----%X",buf[i+19]);

					subdir = buf[i+19] + 31;

					

					if (dotflag == 0)

						traverse(subdir);









			}

			else if (buf[i+1] != 32)

				printf(".");

		}

		count++;

		if (count == 11) {

			count = 0;	

			if (sector != 19)

				i += 21;	

			else i += (32 + 21);

			printf("\n");

		}

	}

	dotflag = 0;



}



void traverse2(int sector, char * dir) {

	unsigned char buf[1000000];

	lseek(fd, sector * SECTOR_SIZE, SEEK_SET);

	read(fd, buf, 14 * SECTOR_SIZE);

	int count = 0;

	int namecount = 0;

	int subdir = 0;

	int subsector;

	int i;

	char subdirname[100];

	

	

	printf("\n");

	for (i = 0; i < 14*SECTOR_SIZE; i++) {

		if (count == 0) { //new entry

			subdir = 0;

			

			if (buf[i] == 0) break; //end of directory			

			

			

			if (buf[i+26] == 0 && buf[i+27] == 0) { //validity

				i += 31;

				continue;

			}

			

			if (buf[i] == 229) {

				i += 31;

				continue;

			}	

			

			else printf("/%s", dir);

			

		

			if ((buf[i+11] >> 4) % 2 == 1) { //subdirectory

				subdir = 1;

				subsector = (buf[i+27] << 8) + buf[i+26] + 31;

				

				

				

			}

		}

	

		if (buf[i] != 32) { //print entry excluding space

			printf("%c", buf[i]);

			if (subdir == 1) {

				subdirname[count] = buf[i];

				namecount++;

//				printf("%d",namecount);

			}

		}	

		if (count == 7)

			if (subdir == 0 && buf[i+1] != 32) 

				printf("."); //print '.' for files

			else if (subdir == 1) {

				printf("%5c\t\t\t\t<DIR>", ' ');

				subdirname[namecount] = '/';

				subdirname[namecount+1] = '\0';

				namecount = 0;

				

				if (buf[i+25] != '.' && buf[i-7] != '.')

					traverse2(subsector, subdirname);

			}

		count++;

		if (count == 11) { //skip details

			count = 0;

			i += 21;

			printf("\n");

			

		}	

		

	}

}





void traversel(int sector, char * dir) {

	printf("\n");

	

	

	unsigned char buf[1000000];

	lseek(fd, sector * SECTOR_SIZE, SEEK_SET);

	read(fd, buf, 14 * SECTOR_SIZE);

	int count = 0;

	int namecount = 0;

	int subdir = 0;

	int subsector;

	int i;

	char subdirname[100];

	

	int year, month, day, hour, min, sec;

	unsigned int filesize;

	int startingcluster;

	

	if (sector == 19) {

		printf("\n");

	

		 printf("   *****************************\n");

		 printf("   ** FILE ATTRIBUTE NOTATION **\n");

		 printf("   **                         **\n");

		 printf("   ** R ------ READ ONLY FILE **\n");

		 printf("   ** S ------ SYSTEM FILE    **\n");

		 printf("   ** H ------ HIDDEN FILE    **\n");

		 printf("   ** A ------ ARCHIVE FILE   **\n");

		 printf("   *****************************\n");

	

	//	printf("\n");

	

	}

	

	for (i = 0; i < 14*SECTOR_SIZE; i++) {

		if (count == 0) { //new entry

			subdir = 0;

			if (buf[i] == 0) break; //end of directory			

			

			if (buf[i] == 229 || ((buf[i+26] == 0 && buf[i+27] == 0))) {

				i += 31;

				continue;

			}	

//			if () { //

//				i += 31;

//				continue;

//			}	

			

			else {

				printf("-"); //attribute notations

				if ((buf[i+11] >> 5) % 2 == 1)

					printf("A");

					else printf("-");

				if ((buf[i+11] >> 1) % 2 == 1)

					printf("H");

					else printf("-");

				if ((buf[i+11] >> 0) % 2 == 1)

					printf("R");

					else printf("-");

				if ((buf[i+11] >> 2) % 2 == 1)	

					printf("S");

					else printf("-    ");

				

				//CHANGED DATE	

				year = 1980 + (buf[i+25] >> 1);

				month = (buf[i+25] % 2) * 8 + (buf[i+24] >> 5);

				day = (buf[i+24] % 32);

				hour = (buf[i+23] >> 3);

				min = ((buf[i+23] & 7) << 3) + ((buf[i+22] >> 5) & 7);

				sec = (buf[i+22] & 31);

				printf("%2d/%02d/%4d %2d:%02d:%02d    ", month, day, year, hour, min, sec);

				

				//FILESIZE

				filesize = (buf[i+31] << 24) + (buf[i+30] << 16) + (buf[i+29] << 8) + (buf[i+28] & 0xFF);

				if (filesize != 0) printf("%5d  \t", filesize);

					else printf("<DIR>   ");

				

				//FILENAME

				printf("/%s", dir);

				

				//STARTING CLUSTER

				startingcluster = (buf[i+26]) + (buf[i+27] * 16);

			}

			

		

			if ((buf[i+11] >> 4) % 2 == 1) { //subdirectory

				subdir = 1;

				subsector = (buf[i+27] << 8) + buf[i+26] + 31;

				

				

				

			}

		}

		

		

	

		if (buf[i] != 32) { //print entry excluding space

			printf("%c", buf[i]);

			if (subdir == 1) {

				subdirname[count] = buf[i];

				namecount++;

//				printf("%d",namecount);

			}

		}	

		if (count == 7)

			if (subdir == 0 && buf[i+1] != 32) 

				printf("."); //print '.' for files

			else if (subdir == 1) {

				//printf("\t\t\t<DIR>\n");

				subdirname[namecount] = '/';

				subdirname[namecount+1] = '\0';

				namecount = 0;

				

				if (buf[i+25] != '.' && buf[i-7] != '.')

					//printf("\n");

					traversel(subsector, subdirname);

			}

			

			

		count++;

		if (count == 11) {

			count = 0;

			i += 21;

			

			printf("%10c\t\t\t%3d\n", ' ', startingcluster);

			

		}	

		

	}



}



struct dirent {

	unsigned char name[11];

//	unsigned char attribute;

	unsigned char reserved[11];

	unsigned char time[2];

	unsigned char date[2];

	unsigned char cluster1;

	unsigned char cluster2;

	unsigned char size[4];

};





void showfile(char * search) {



	int i;

	int foundflag = 0;

	int fd = open("imagefile.img", O_RDONLY,0);

	struct dirent rootentries[50];

	lseek(fd, 19 * SECTOR_SIZE, SEEK_SET);

	read(fd, rootentries, 3 * SECTOR_SIZE);

	

	for (i = 0; i < 224; i++) {



		char *a = rootentries[i].name;



	//	char *c = rootentries[i].attribute;

		char *d = rootentries[i].reserved;

		char *e = rootentries[i].time;

		char *f = rootentries[i].date;

		char g = rootentries[i].cluster1;

		char h = rootentries[i].cluster2;

		

//		printf("Searching...%s\n",rootentries[i].name);

	

		if (!strcmp(search, rootentries[i].name)) {

			foundflag = 1;

			int startingcluster = (rootentries[i].cluster1) + (rootentries[i].cluster2 * 16) + 31;

			showsector(startingcluster);

			break;

			}

	

	}

	

	if (foundflag == 0)

		printf("file not found\n");



	

}

void flop() {
   char t;
   setfloppy++;     	//toggle floppy mounted boolean and automount floppy
   autoMount();
   userinput++;
   
   	/* copies current args from apash main to floppy.c's input string   */	
	if(strstr(inputargs[0], "help")!= NULL){
		help();
		
	}
	
	/*if(strstr(inputargs[0], "fmount")!= NULL) {
		if(setfloppy != 0) {		
			printf("Floppy already mounted. Unmount current floppy if you wish to mount a new floppy.");
			continue;
		} else {
			input2 = getArguments(input);
			fmount(input2); //will create a string with no spaces then point to the part after "fmount"
			continue;	
		}
	}

	if(strstr(inputargs[0], "fumount")) {
		if(setfloppy != 0) {		
			fumount(); 
			continue;
		} else {	
		 	printf("There is no floppy mounted.\n");
			continue;	
		}	
	}*/
	

	if(strstr(inputargs[0], "structure")) {
		if(setfloppy != 0) {		
			structure(); 
			
		} else {	
		 	printf("There is no floppy mounted.\n");
				
		}	
	}

		
	if(strstr(inputargs[0], "showsector")) {
		if(setfloppy != 0) {
			strncpy(input2, getArguments(input), sizeof(input2)); //input2 = getArguments(input);
			int newinput = string2Int(input2);
			showsector(newinput);
			
		} else {	
		 	printf("There is no floppy mounted.\n");
				
		}	
	}

	if(strstr(inputargs[0], "showfat")) {
		if(setfloppy != 0) {		
			showfat(); 
			
		} else {	
		 	printf("There is no floppy mounted.\n");
				
		}	
	}

	if(strstr(inputargs[0], "traverse")) {
		if(setfloppy != 0) {
			strncpy(input2, getArguments(input), sizeof(input2));
			if (strcmp(input2,"-l") == 0)
				traversel(19, "");
			else traverse2(19, "");
			
		} else {
		 	printf("There is no floppy mounted.\n");
				
		}	
	}
	
	if(strstr(inputargs[0], "showfile")) {
		if(setfloppy != 0) {
			strncpy(input2, getArguments(input), sizeof(input2));
			strncpy(input2, formatName(input2), sizeof(input2));   //input2 = formatName(input2);
			showfile(input2);
			
		} else {
		 	printf("There is no floppy mounted.\n");
				
		}	
		
	}

   //memset(input, 0, sizeof(input));
   //memset(input2, 0, sizeof(input2));



}


