#include <ctype.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <floppy.h>
#include <apash.h>

/* Some variables */
char * flopcommands[10] = {"help", "quit", "structure", "traverse", "showfat", "showsector", "showfile", "fmount", "fumount", "test"};
char inputsymbols[] = "<>|";
int pipeArray[9];
int fd1, fd2;
char inputsymbols2[] = "\".'-123456789";
char * argx[6];
char argw[6][PATH_MAX];
char PATH[512][PATH_MAX];
int status = 2;
int calls, fdA, fdB, fdC;
int sizeofpath = 0;		//size of path variable so iterations know when to stop. 
int wasfound =0;    //supposed to be a boolean check if any command was executed so we know when to print "not found"


/*  //manual flush of std buffer    */
void flushInput() {
	//memset(inputbuffer, 0, sizeof(inputbuffer));   //moved this function to reeInputArgs()
	while((c = getchar()) != '\n' && c != EOF); 

}


/* send args to some struct interpret and execute!  */
void findArguments() {
	char * newargs = malloc(sizeof(PATH_MAX));
	int j;
	
	/*copy contents of inputbuffer for future use  */
	memset(currentbuffer, 0, sizeof(currentbuffer));
	strncpy(currentbuffer, inputbuffer, sizeof(inputbuffer));
		
	/* get first word of arguments, first checks to see if first char is a letter if not, invalid input */
	if (strcspn(inputbuffer, " ") < 2) {
		//inputError1();	
	} else {	
		strncpy(inputargs[0], inputbuffer, strcspn(inputbuffer, " "));
	}
	
	/* now use strtok to copy rest of args to char * array	 */
	newargs = strtok(inputbuffer, " ");
	for(j=0; newargs != NULL;j++) {         
		strncpy(inputargs[j], newargs, strcspn(newargs, " "));
		newargs = strtok(NULL, " ");
	}
	

}


/* clears input then reads from stdin and sends arguments to...? IF appicable if not tells main no input. */
int getInput() {
	
	scanf("%[^\n]s", inputbuffer); 	//get 4095 chars max -1 for null terminator
	strncpy(input, inputbuffer, sizeof(input));  //copy input to floppy.c before getting args
	findArguments(inputbuffer);
		
	
	//printf("%s", inputbuffer);     //test print 
	/* new line check NOT NEEDED?
	if (inputargs[0]) printf("\n"); */

	flushInput();			//args are stored and executed now flush stdin.	
}


/* initialize prompt and get working directory. */
void getPrompt() {
	memset(prompt, 0, sizeof(prompt));  //delete old prompt
	char * cdirectory = malloc(sizeof(PATH_MAX));
		
	getcwd(cdirectory, PATH_MAX);
	gethostname(hostname, 1023);

	/* append strings for hostname, directory and some additional characters to create our dynamic prompt */
	strncat(prompt, hostname, PATH_MAX);
	strncat(prompt, "@", PATH_MAX);
	strncat(prompt, cdirectory, PATH_MAX);	
	strncat(prompt, ">", PATH_MAX);
	
	strncpy(currentdir, cdirectory, PATH_MAX);
	//free or delete used char *'s
	memset(cdirectory, 0, sizeof(cdirectory)); 
	
}


/* iterates current argument struct and executes do fork()'ing here i suppose... probably better to fork() from main? */
int interpreter() {

	int c_pid,l = -1;

	l = forFloppy();
	
	if (l > 0) {
		wasfound++;
		c_pid = fork(); 	
			if(c_pid == 0)	{
				flop();
				sleep(0.5);
				exit(2);
			} else  if(c_pid > 0) {

				wait(&status);        		//Parent waits for child.
			} else {				//end of floppy function check			
				perror("Could not create new process");
				_exit(2);
			}
	}
	
	
	/* if floppy command not found search for system commands*/ 
	if (l == -1 && wasfound < 1) { 		
		forSystem();
	
	}
	
}

/* looks for system commands and if found fork and exec accordingly, dynamically builds argv via buildArgArray function */
int forSystem() {
	int e, c_pid;
 		
	c_pid = fork();

	/*   */

	/* for child  */
	if (c_pid == 0) {



		for (e = 0;e < sizeofpath ;e++) {
			buildArgArray(e);

			/*using execl system call last argument must be null so it knows that's end of argumemts*/
			if (execv(argx[0], argx) != -1) {
				puts("you never get here do you?");
				wasfound += 1;			
				sleep(0.01);
				exit(2);
			}
		}
	
	/* shouldn't make it here, if you do just exit back to parent. */
	return 0;

	}


	/* for parent */
	if(c_pid > 0) {
		wasfound += 1;	
		wait(&status);        //Parent waits for child.
	}
		

}

/* nullifies a pointer */
void nullPointer(char *d) {
	d = NULL;

}

/* appends a null terminator on a string */
void nullTerminate(char *s) {
	int l = strlen(s);	
	s[l+1] = '\0';

}


/* makes an arguments array for execv for the given commands in inputargs array
then assign argw array to argx pointers since execv only takes const char* args anything not assigned is left null
*/
int buildArgArray(int a) {
	int  l, p, o;
	int m = 75;			//an arbitray number really, I could just check all 512 lines of inputargs i guess.

	/* copy the absolute path of program to args array */
	strcpy(argw[0], PATH[a]);
	strcat(argw[0], "/");	
	strcat(argw[0], inputargs[0]);			//argu[0] now says /"path"/"argument"
	argx[0] = argw[0];

	/* copy any special arguments for the program being launched (leave empty most times)*/
	for (l = 0;l < m;l++){
		if (strpbrk(inputargs[l], inputsymbols2) != NULL) {
			strcpy(argw[1], inputargs[l]);
			argx[1] = argw[1];
			p = 10;
			o = l;
		} 
	}		// else, simply leave null


	/* a second check for special arguments */
	/*for (l = 0;l < m;l++){
		if (strpbrk(inputargs[1],inputsymbols) == NULL && p > 0) {
			strcpy(argw[2], inputargs[2]);
			argx[2] = argw[2];

		} 
	}*/

		/* a second check for special arguments */
	for (l = 0;l < m;l++){
		if (p == 10 && strpbrk(inputargs[1],inputsymbols) == NULL) {
			strcpy(argw[1], inputargs[1]);
			argx[1] = argw[1];

		} 
	}






	/* copy the path to args array */	
	strcpy(argw[2], PATH[a]);			//argu[1] now has name of file being searched
	//argx[2] = argw[2];

	/* handle input redirection.*/
	redirectionCheck();

	//printArgsArray();

	


}


void redirectionCheck() {
	int x, y;
	x = 75;
	
	/* check for < */
	for (y = 0; y < x; y++){
		if (strchr(inputargs[y], '<') != NULL) {
			fd1 = open(inputargs[y+1], O_RDONLY,0);	//open a file descriptor for file in question
			dup2(fd1, STDIN_FILENO);			//make that file get mapped to stdin (fd = 0);
		
		} 
	}


	/* check for > */
	for (y = 0; y < x; y++){
		if (strchr(inputargs[y], '>') != NULL) {
			fd2 = open(inputargs[y+1], O_WRONLY,0);	//open a file descriptor for file in question
			dup2(fd2, STDOUT_FILENO);			//make that file get mapped to stdin (fd = 0);
		
		} 
	}

}


/* check for floppy commands */
int forFloppy(){
	int j;
	for(j = 0; j < 10;j++) {
		if(strcmp(inputargs[0], flopcommands[j]) == 0){
			return  2;
		} 
	}

	return -1;
}

/* frees memory for malloc'd variables in apash.c (call from main)     */
void freeVariables() {
	//free(cdirectory);


}


/* prints the currrent contents of inputargs   */
void printArguments() {
	int k;

	for (k=0; inputargs[k][0] ;k++ ) {
		printf("%s\n", inputargs[k]);
	}


}

/* prints the currrent contents of argx array or others if changed */
void printArgsArray() {
	int k;
	printf("\nargx index 0 is: %s", argx[0]);
	printf("\nargx index 1 is: %s", argx[1]);
	printf("\nargx index 2 is: %s", argx[2]);
	printf("\nargx index 3 is: %s", argx[3]);
	printf("\nargx index 4 is: %s", argx[4]);
	printf("\nargx index 5 is: %s\n___________________________________\n", argx[5]);
	
	//printf("\nargx index 4 is: %s", arg1+3);
	//printf("\n\nargw index 0 is: %s", argw[0]);
	//printf("\nargw index 1 is: %s", argw[1]);
	//printf("\nargw index 2 is: %s", argw[2]);
	//printf("\nargw index 3 is: %s", argw[3]);
	//printf("\n\npath index 0 is: %s\n", PATH[0]);
	//printf("\ninputargs index 3 is: %s", argu+3);

	/*for (k=0; k < 12 ;k++ ) {
		printf("array contents: %d\n", arrayA[k]);
	}*/


}

/* prints the currrent contents of PATH Variable*/
void printPathVar() {
	int k;
	
	printf("Current Paths separated by ':' are:\n\n");
	for (k=0; k < sizeofpath ;k++ ) {
		printf("%s:", PATH[k]);
	}
	
	printf("\n\n");
}


void inputError1() {
	printf("first token entered must be an alphanumeric command type \"help\" to see commands");
}

void inputError2() {
	printf("\ncommand not found. Type \"help\" to see valid commands.\n\n");

}


/* Clears input args array   */
void freeInputArgs() {
	int y;
	for (y = 0;y < 512 ;y++) {	
		memset(inputargs[y], 0, PATH_MAX);
	}
	
	memset(inputbuffer, 0, sizeof(inputbuffer));

	/* also clear some important int values wasfound, calls, etc. */
	wasfound = 0;


	if(fd1 > 0) { close(fd1); }
	if(fd2 > 0) { close(fd2); }
}


/* To reduce the size of "interpreter method, moving built in functions check to separate method."  */
void builtIn() {
		
	/* quit the shell */
	if(!strcmp(inputargs[0], "quit") || !strcmp(inputargs[0], "exit")){
		is_args = 0;
		wasfound += 1;
	}

	/* change the directory*/
	if(!strcmp(inputargs[0], "cd")){
		chdir(inputargs[1]);
		wasfound += 1;
	}
	
	/* PATH var change */
	if(!strcmp(inputargs[0], "path")){
		if((strchr(currentbuffer, '+') != NULL)  || strchr(currentbuffer, '-') != NULL) {
			changePath();
			wasfound += 1;


		} 
		if((strchr(currentbuffer, '+') == NULL)  && strchr(currentbuffer, '-') == NULL)  {
			printPathVar();
			wasfound += 1500;

		} 
		if((strchr(currentbuffer, '+') == NULL)  && (strchr(currentbuffer, '-') == NULL) && (sizeofpath < 1)){
			puts("\nNo path variable set.\n");
			wasfound += 1;
		}
			

	} 

	/* test function for laziness*/
	if(!strcmp(inputargs[0], "-test-")){
		thisISATest();
		wasfound += 1;
	}
	
	
	//after if's checked go to interpreter.
}

/* returns size of path var to main*/
int getWasFound() {
	return wasfound;
}

/* checks to see if any input was entered (check for first 100 bytes of said buffer)*/
int inputCheck(char * line){
	char z, y;

	for(y = 0;y < 99;y++) {
		char z = line[y];
		if (isalnum(z) > 0){
			return 25;
		}
	}

	return -99;
}


/*appends path variable as per Wangs requirment path + and path -  */
int changePath(){
	int h;
	/* add to current path if path is empty */
	if ((strchr(currentbuffer, '+') != NULL) && sizeofpath == 0) {
		sprintf(PATH[0], "%s", inputargs[2]);
		sizeofpath++;
		printf("added %s to path.\n", inputargs[2]);
		return 0;
	/* add to current path if path has 1 or more items */		
	}else if((strchr(currentbuffer, '+') != NULL) && sizeofpath > 0) { 
		if(strpbrk(inputargs[1], "+") != NULL) {
			sprintf(PATH[sizeofpath], "%s", inputargs[2]);
			sizeofpath++;
			printf("added %s to path.\n", inputargs[2]);
			return 0;
		}
	/* delete from path */
	} else if((strchr(currentbuffer, '-') != NULL) && sizeofpath > 0) {
		for (h=0; h < sizeofpath ;h++ ) {
			if(strcmp(PATH[h], inputargs[2]) == 0) {
				deletePath();
				return 0;
			}					
		}
		
		printf("Path not found\n");
		return 0;
	}	

	printf("Path not found\n");

}


/*deletes the requested path by copying PATH var to new array sans said path*/
int deletePath() {
	int m;	
	char a[512][PATH_MAX];

	/* copy contents to new array */
	for (m=0; m < sizeofpath ;m++ ) {
		if(strcmp(PATH[m], inputargs[2]) != 0) {
			strcpy(a[m], PATH[m]);
		}
	}
	
	/* clear old path variable */
	for (m = 0;m < sizeofpath ;m++) {	
		memset(PATH[m], 0, PATH_MAX);
	}

	/* copy contents back to PATH  */
	for (m=0; m < sizeofpath ;m++ ) {
	
			strcpy(PATH[m], a[m]);

	}

	//sizeofpath--;
	printf("%s deleted.\n", inputargs[2]);


}


/* this is a system test function for debugging only! will mount several linux directories (type "-test-")  */
void thisISATest(){
	 char GGPATH[512][PATH_MAX]= {"/lib","/sbin","/bin","/usr/games","/usr/sbin","/lib/systemd","/usr/bin"};
	 int b, k;
	
	b = sizeofpath;
	sizeofpath += 7;
	/* copy contents to PATH  */
	for (; b < sizeofpath ;b++ ) {
	
			strcpy(PATH[b], GGPATH[b]);

	}

	/* add current directory to path as well */
	sprintf(PATH[sizeofpath], "%s", currentdir);
			sizeofpath++;
	
	/* print new path */
	printf("\n\nPath variable appended. New Path:\n\n");
	for (k=0; k < sizeofpath ;k++ ) {
		printf("%s:", PATH[k]);
	}
	printf("\n\n");

}






